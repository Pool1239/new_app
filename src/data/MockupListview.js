
export  const mockup =
[{
    type: 'Annual Leave',
    typeThai: '   ลาพักร้อน',
    day: '' ,
    ds:  'Days',
    isBenefit:false
},
{
    type: 'Personal Leave',
    typeThai: '    ลากิจ',
    day: '',
    ds:  'Days'
    ,
    isBenefit:false
},
{
    type: 'Sick leave',
    typeThai: '     ลาป่วย',
    day: '',
    ds:  'Days'
    ,
    isBenefit:false
},
{
    type: 'Flexlble Benefit Request',
    typeThai: '     คำขอสวัสดิการอื่่นๆ ',
    day: '',
    ds:  '',
    isBenefit:true
},
{
    type: 'Enter for monkhodd',
    typeThai: '    ลาบวช',
    day: '',
    ds:  'Days',
    isBenefit:false
},
{
    type: 'Military Leave',
    typeThai: '    ลาเพื่อรับราชการทหาร',
    day: '',
    ds:  'Days',
    isBenefit:false
},
{
    type: 'Maternity Leave',
    typeThai: '    ลาคลอด',
    day: '',
    ds:  'Days',
    isBenefit:false
},
{
    type: 'Training Leave',
    typeThai: '      ลาเพื่อฝึกอบรม',
    day: '',
    ds:  'Days',
    isBenefit:false
},


]

