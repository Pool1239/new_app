import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, Image, TextInput,TouchableOpacity,AsyncStorage } from 'react-native';
import { Container, Header, Left, Right, Button, Icon, Title, Card, CardItem, Body, Content } from 'native-base';
import { get, post, sever } from '../service/api'


export default class ListLeave extends React.Component {
  
  
  constructor(props) {
    super(props)
    this.state = {
      userdata: [],
      annual: [], timeannual:[], sumannual:'',
    }
  }

  async componentDidMount() {
    let res = await AsyncStorage.getItem("userdata")
    let userdata = JSON.parse(res)
    // this.setState({userdata})
    // console.log('nobar', userdata)
    if (userdata) {
      this.setState({ userdata })
    } else {
      this.setState({ userdata: [] })
    }

 
  }

  onPressLogin() {
    if (this.st == 'Flexlble Benefit Request') 
    { alert("กรุณากรอกข้อมุลให้ครบ"); }
    else 
    { navigate('ap3') }
}

  render() {
    const { leave,navigate } = this.props
    return (
      <TouchableOpacity style={{ width: 350, height: 120, backgroundColor: '#F5F5F5' }} onPress={()=>leave.isBenefit ? navigate('ap4'):navigate('ap3')}>
        <Card  >
          <CardItem >
            <Text style={{ fontSize: 20, }} >
              {leave.type}
            </Text>
            <Text style={{ fontSize: 25, color: '#00CCFF' }} >
              {this.state.sumannual + "10" }
            </Text>
            <Text style={{ fontSize: 20, }} >
            {leave.ds}
          </Text>
          </CardItem>
          <Text style={{ fontSize: 20, }} >
            {leave.typeThai}
          </Text>    
        </Card>
      </TouchableOpacity> 
    );
  }
}

