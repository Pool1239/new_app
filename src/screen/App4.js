import React, { Component } from 'react';
import { View, Text, FlatList, styles, textRegister, TouchableOpacity, AsyncStorage, Image, TouchableHighlight, Dimensions } from 'react-native'
import { Container, Header, Left, Right, Icon, Title, Body, Content, Picker, Button, Card, CardItem, Input } from 'native-base';
import DatePicker from 'react-native-datepicker'
import { get, post, sever } from '../service/api'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import PhotoUpload from 'react-native-photo-upload';
var { height, width } = Dimensions.get('window');

export default class App extends Component {
  static navigationOptions = {
    // title: 'Login',
    header: null,
  }


  constructor(props) {
    super(props);
    this.state = {
      ben_type: '',
      // ben_date_request: '',
      ben_date_receipt: '',
      amount: '',
      files: [],
      avatar: '',
    }
  }

  onValueChange3(value = string) {
    this.setState({
      selected3: value
    });
  }
  async onPressA() {
    if (this.state.ben_type == "" || this.state.ben_date_receipt == "" || this.state.amount == "" || this.state.avatar == ""
    ) { alert("กรุณากรอกข้อมูลให้ครบ"); }
    else {

      let ress = await AsyncStorage.getItem("userdata")
      let userdata = JSON.parse(ress)
      this.setState({
        userdata: userdata,

      })
      const empID = parseInt(userdata.result[0].empID)

      const { ben_type, ben_date_receipt, amount, avatar } = this.state
      const obj = {
        empID, ben_type, ben_date_receipt, amount, image: ',' + avatar
      }
      try {
        await post('/insert_flexible', obj, userdata.token)
        alert('create success')
        this.props.navigation.navigate('ap2')
      } catch (error) {
        alert(error)
      }
    }
  }

  render() {
    return (

      <View style={{ backgroundColor: '#FFFFFF', flex: 1 }} >

        <Text style={{ fontSize: 20, color: 'white', backgroundColor: '#00CCFF', textAlign: 'center', paddingTop: 7, height: 50 }} >Flexible Benefit Request</Text>



        <KeyboardAwareScrollView keyboardShouldPersistTaps={'handled'}>
          <View style={{ flexDirection: 'column', flex: 15, paddingHorizontal: 40, }} >


            <View style={{ marginTop: 10 }}>
              <Text style={{ fontSize: 15, color: '#00CCFF', borderBottomWidth: 1, paddingBottom: 5, paddingLeft: 10 }}>Flexible Benefit Request </Text>

              <View style={{ flexDirection: 'row', borderWidth: 2, marginTop: 10, borderRadius: 3.5, borderColor: '#bbb' }}>
                <Picker style={{ height: 45, borderRadius: 3.5 }}
                  mode="dropdown"
                  selectedValue={this.state.ben_type}
                  onValueChange={(f) => this.setState({ ben_type: f })} >
                  <Picker.Item label="Perpose for Flexible Benefit" value="" />
                  <Picker.Item label="Optical Purchasing" value="1" />
                  <Picker.Item label="Oral Purchasing" value="2" />
                  <Picker.Item label="Sport" value="3" />
                  <Picker.Item label="Transportation on Vacation" value="4" />
                </Picker>
              </View>

              <View style={{ borderWidth: 1, borderRadius: 3.5, marginTop: 10, borderColor: '#bbb' }}>
                <DatePicker underlineColorAndroid='transparent' placeholder="Date on Receipt" style={{ width: '100%' }}
                  date={this.state.ben_date_receipt} mode='datetime' confirmBtnText='confirm' cancelBtnText='cancel'
                  customStyles={{
                    dateIcon: {
                      position: 'absolute',
                      right: 0,
                      top: 4,
                      marginLeft: 0
                    },
                    dateInput: {
                    }
                  }}
                  onDateChange={(d) => this.setState({ ben_date_receipt: d })}
                />
              </View>

              <View style={{ flexDirection: 'row', borderWidth: 2, marginTop: 10, justifyContent: 'space-between', alignItems: 'center', borderRadius: 3.5, borderColor: '#bbb', height: 50 }}>
                <View style={{ marginLeft: 10 }}>
                  <Text>Amount</Text>
                </View>
                <View >
                  <Input keyboardType='numeric' placeholder='0.00' style={{ width: '100%', marginRight: 20 }}
                    value={this.state.amount} onChangeText={(b) => this.setState({ amount: b })} />
                </View>
              </View>
            </View>

            <PhotoUpload
              onPhotoSelect={avatar =>  {
                if (avatar) {
                  console.log('Image base64 string: ', avatar)
                  this.setState({
                    avatar
                  })
                  
                }
              }}
            >
              {
                this.state.avatar ?
                  <Image
                    style={{
                      marginVertical: 20,
                      width: 250,
                      height:200,
                      resizeMode: 'contain'
                    }}
                  />
                  :
                  <Image
                    style={{
                      marginVertical: 30,
                      width: 100,
                      height: 100,
                    }}
                    source={require('../assets/camera.png')}
                  />
              }


            </PhotoUpload>


            <View style={{ flexDirection: 'row', marginTop: 20 }} >
              <Button style={{ justifyContent: 'center', backgroundColor: 'white', borderRadius: 5,borderColor: '#00E0FF', width: '47.5%', marginRight: '2.5%', borderWidth: 2 }}
                onPress={() => this.props.navigation.navigate('ap2')} ><Text style={{color:'#00E0FF'}}>Cancel</Text></Button>

              <Button style={{ justifyContent: 'center', backgroundColor: 'white', borderRadius: 5, borderColor: '#00E0FF', width: '47.5%', marginLeft: '2.5%', borderWidth: 2 }}
                onPress={() => this.onPressA()}><Text style={{color:'#00E0FF'}}> Submit</Text ></Button>
            </View>


          </View>

        </KeyboardAwareScrollView>
        <View style={{ alignItems: 'center', backgroundColor: '#00CCFF', justifyContent: 'center',height:50 }} >
          <Text style={{ fontSize: 15, color: 'white' }} >PROJECTSOFT (Thailand)Co,Ltd.</Text>
        </View>
      </View>


    );
  }
}


