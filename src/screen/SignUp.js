import React, { Component } from 'react';
import {TextInput}from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { Container, Header, Content, Button, Text, Footer, View,Form,Item,Label,Input, Picker,Card,CardItem} from 'native-base';
export default class ButtonOutlineExample extends Component {
  static navigationOptions = {
    // title: 'Login',
    header: null,
}

  constructor(props) {
    super(props);
    this.state = {
      selected3: "key0",
    };
  }
  onValueChange3(value=string) {
    this.setState({
      selected3: value
    });
  }
  render() {

    
    return (
      <KeyboardAwareScrollView contentContainerStyle={{ flex: 1 }}>
      <Container style={{backgroundColor:'#FFFFFF'}}>
      
            <View style={{flex:1,justifyContent:'space-around',alignItems:'center',flexDirection:'column'}}>
            <View><Text style={{fontSize: 20,
                color: '#33B2FF',
                textAlign: 'center',}}>Sign Up</Text>
                </View>

                <View style={{alignItems:'center',flexDirection:'column'}}>
                
                <Form>
                      <Text style={{color:'#666666'}}>Name</Text>
                      <Item rounded style={{width:250,height:40}}>
                      <Input/>
                      </Item>

                      <Text style={{color:'#666666'}}>Lastname</Text>
                      <Item rounded style={{width:250,height:40}}>
                      <Input/>
                      </Item>

                      <Text style={{color:'#666666'}}>Staff ID</Text>
                      <Item rounded style={{width:250,height:40}}>
                      <Input/>
                      </Item>

                            <Text style={{color:'#666666'}}>sex</Text>
                            <View style={{flexDirection:'row'}}>
                            <Card>
                            <CardItem style={{height:40 ,width:undefined}}>
                              <Picker

                                mode="dropdown"
                                style={{ width: undefined , color:'#666666' }}
                                selectedValue={this.state.selected3}
                                onValueChange={this.onValueChange3.bind(this)} >
                                <Picker.Item label="Male" value="key0" />
                                <Picker.Item label="Female" value="key1" />
                              </Picker>
                            </CardItem>
                            </Card>
                            </View>
                            
                      <Text style={{color:'#666666'}}>Start date of working</Text>
                      <Item rounded style={{width:250,height:40}}>
                      <Input/>
                      </Item>
                  </Form>
                </View>

                <View>
                <Button rounded light style={{width:200 , height:50, borderWidth: 2,borderColor: '#00E0FF',
                        backgroundColor: 'white',
                        justifyContent: 'center',
                        alignSelf: 'center',
                        marginBottom: 1}}
                        onPress={() => this.props.navigation.navigate('p')}>
                <Text style={{color:'#00E0FF'}}>Next</Text>
                </Button>
                </View>
            </View>
          
          
          
        
      </Container>
      </KeyboardAwareScrollView>
    );
  }
}

