import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, Image, TextInput, Linking } from 'react-native';
import { Button } from 'native-base'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
export default class ForgotPassword extends React.Component {

    static navigationOptions = {
        // title: 'Login',
        header: null,
    }
    constructor(props) {
        super(props)
        this.state = {
            username: "",
            password: "",
            confirmpassword: "",
        }
    }
    onPressLogin() {
        if (this.state.username == "" || this.state.password == "" || this.state.confirmpassword == "") { alert("กรุณากรอกข้อมูลให้ครบ"); }
        else {
            alert("Success");
            this.props.navigation.navigate('d1')
        }
    }

    render() {
        return (
            <KeyboardAwareScrollView contentContainerStyle={{ flex: 1 }} keyboardShouldPersistTaps={'handled'}>
                <View style={{flex:1,justifyContent:'center',paddingHorizontal:75}}>
                    <Text style={styles.welcome1}>
                        Create Password
                     </Text>

                    <Text style={{ marginTop: 30 }} >
                        Username (ProjectSoft Email address)
                     </Text>
                    <TextInput style={styles.tp1} underlineColorAndroid='transparent'
                        value={this.state.username}
                        onChangeText={(e) => this.setState({ username: e })}
                        onSubmitEditing={() => { this.secondTextInput.focus(); }}
                    />
                    <Text >
                        Password
                     </Text>
                    <TextInput style={styles.tp} underlineColorAndroid='transparent' secureTextEntry={true}
                        value={this.state.password}
                        onChangeText={(e) => this.setState({ password: e })}
                        ref={(input) => { this.secondTextInput = input; }}
                        onSubmitEditing={() => { this.secondTextInput2.focus(); }}
                    />
                    <Text  >
                        Confirm Password
                     </Text>
                    <TextInput style={styles.tp} underlineColorAndroid='transparent' secureTextEntry={true}
                        value={this.state.confirmpassword}
                        onChangeText={(e) => this.setState({ confirmpassword: e })}
                        ref={(input) => { this.secondTextInput2 = input; }}
                    />

                    <Button style={styles.bt} onPress={() => this.onPressLogin()}>
                        <Text style={{ color: '#00E0FF', fontSize: 20 }}>Submit </Text>
                    </Button>

                </View>
            </KeyboardAwareScrollView>
        );
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        backgroundColor: '#F3F3F3',
    },

    welcome: {
        fontSize: 15,
        color: '#33B2FF',
        textAlign: 'center',
        margin: 50,
    },
    welcome1: {
        fontSize: 20,
        color: '#33B2FF',
        textAlign: 'center',
        margin: 20,
    },
    tp: {
        alignSelf: 'center',
        width: '100%',
        height: 40,
        backgroundColor: "#FFFFFF",
        marginBottom: 10,
        color: 'black',
        borderColor: '#CBCBCB',
        borderWidth: 1,
        paddingLeft: 20

    },
    tp1: {
        alignSelf: 'center',
        width: '100%',
        height: 40,
        backgroundColor: "#FFFFFF",
        marginBottom: 10,
        borderColor: '#CBCBCB',
        borderWidth: 1,
        paddingLeft: 20

    },
    bt: {
        borderWidth: 2,
        borderColor: '#00E0FF',
        backgroundColor: 'white',
        width: '100%',
        height: 40,
        justifyContent: 'center',
        alignSelf: 'center',
        marginTop: 50
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
});