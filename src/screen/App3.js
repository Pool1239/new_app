import React, { Component } from 'react';
import { View, Text, FlatList, o, TextInput, Linking, AsyncStorage, ScrollView } from 'react-native'
import { Container, Header, Left, Right, Icon, Title, Body, Content, Picker, Button, Card, CardItem } from 'native-base';
import DatePicker from 'react-native-datepicker'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { get, post, sever } from '../service/api'

export default class App3 extends Component {
  static navigationOptions = {
    header: null,

  }
  constructor(props) {
    super(props)
    this.state = {
      time_from: '',
      time_to: '',
      date_time: '',
      lea_day: '1',
      date_start: '',
      date_end: '',
      lea_reason: '',
      an: [],
      empID: []
    }
  }
  async onPressSubmit() {
    if (this.state.lea_day == 1) {
      if (this.state.lea_reason == "" || this.state.lea_day == "" || this.state.date_start == "" || this.state.date_end == "") {
        alert("กรุณากรอกข้อมุลให้ครบ");
      }
      else {
        let res = await AsyncStorage.getItem("an")
        let leaID_type = JSON.parse(res)
        let ress = await AsyncStorage.getItem("userdata")
        let userdata = JSON.parse(ress)
        this.setState({
          userdata: userdata,
          leaID_type: leaID_type,
        })
        const empID = parseInt(userdata.result[0].empID)
        const lea_type = parseInt(leaID_type[0].leaID_type)
        const { lea_day, date_start, date_end, lea_reason } = this.state
        const obj = {
          empID, lea_type, lea_day, date_start, date_end, lea_reason
        }

        try {
          await post('/insert_leave', obj, userdata.token)
          alert('create success')
          this.props.navigation.navigate('ap2')
        } catch (error) {
          alert(error)
        }
      }

    }
    else {
      if (this.state.lea_reason == "" || this.state.lea_day == "" || this.state.date_time == "" || this.state.time_from == "" || this.state.time_to == "") {
        alert("กรุณากรอกข้อมูลให้ครบ");
      }
      else {
        let res = await AsyncStorage.getItem("an")
        let leaID_type = JSON.parse(res)
        let ress = await AsyncStorage.getItem("userdata")
        let userdata = JSON.parse(ress)
        this.setState({
          userdata: userdata,
          leaID_type: leaID_type,
        })
        const empID = parseInt(userdata.result[0].empID)
        const lea_type = parseInt(leaID_type[0].leaID_type)
        const date_start = this.state.date_time + ' ' + this.state.time_from
        const date_end = this.state.date_time + ' ' + this.state.time_to
        const { lea_day, lea_reason } = this.state
        const obj = {
          empID, lea_type, lea_day, date_start, date_end, lea_reason
        }

        try {
          await post('/insert_leave', obj, userdata.token)
          alert('create success')
          this.props.navigation.navigate('ap2')
        } catch (error) {
          alert(error)
        }

      }
    }
  }


  async componentDidMount() {
    let res = await AsyncStorage.getItem("an")
    let userdata = JSON.parse(res)

    this.setState({
      an: userdata[0].leaName_type,
    })
  }

  render() {
    return (



      <View style={{ backgroundColor: '#FFFFFF', flex: 1 }} >
        <Text style={{ fontSize: 20, color: 'white', backgroundColor: '#00CCFF', textAlign: 'center', paddingTop: 7, height: 50 }} >Flexible Benefit Request</Text>

        <KeyboardAwareScrollView keyboardShouldPersistTaps={'handled'}>
          <View style={{ flexDirection: 'column', paddingHorizontal: 40, flex: 15 }} >
            <Text style={{ marginTop: 10, fontSize: 15, color: '#00CCFF', borderBottomWidth: 1, paddingBottom: 5, paddingLeft: 10 }}>Leave Request</Text>

            <View style={{ marginTop: 20 }}>
              <Text style={{ borderWidth: 2, marginBottom: 10, fontWeight: 'bold', paddingTop: 10, fontSize: 17, height: 45, paddingLeft: 10, borderRadius: 3.5, borderColor: '#bbb' }}>
                {this.state.an}
              </Text>

              <View style={{ flexDirection: 'row', borderWidth: 2, borderRadius: 3.5, borderColor: '#bbb' }}>
                <Picker style={{ height: 45, borderRadius: 3.5 }}
                  mode="dropdown"
                  selectedValue={this.state.lea_day}
                  onValueChange={(f) => this.setState({ lea_day: f })} >
                  <Picker.Item label="Fullday" value="1" />
                  <Picker.Item label="Halfday" value="2" />
                </Picker>
              </View>

              {/* <Card style={{ width: 302, marginTop: 10, borderColor: '#000000', height: 40 }}>
              <CardItem style={{ height: 50 }}>
                <Picker style={{ height: 50 }}
                  selectedValue={this.state.lea_day}
                  onValueChange={(f) => this.setState({ lea_day: f })}
                  mode="dropdown"
                >
                  <Picker.Item label="Full day" value="1" />
                  <Picker.Item label="Half day" value="2" />

                  
                </Picker>
              
              </CardItem>
            </Card> */}
              {this.state.lea_day == 1 ?

                <View style={{ flexDirection: 'row', marginTop: 10 }}>
                  <View style={{ borderWidth: 1, borderRadius: 3.5, borderColor: '#bbb', width: '47.5%', marginRight: '2.5%' }}>
                    <DatePicker style={{ width: '100%' }} date={this.state.date_start} mode="datetime"

                      placeholder="from"
                      confirmBtnText="Confirm"
                      cancelBtnText="Cancel"
                      customStyles={{
                        dateIcon: {
                          position: 'absolute',
                          right: 0,
                          top: 4,
                          marginLeft: 0
                        },
                      }}
                      onDateChange={(d) => { this.setState({ date_start: d }) }}
                    /></View>
                  <View style={{ borderWidth: 1, borderRadius: 3.5, borderColor: '#bbb', width: '47.5%', marginLeft: '2.5%' }}>
                    <DatePicker date={this.state.date_end} mode="datetime"
                      style={{ width: '100%' }}
                      placeholder="To"
                      confirmBtnText="Confirm"
                      cancelBtnText="Cancel"
                      customStyles={{

                        dateIcon: {
                          position: 'absolute',
                          right: 0,
                          top: 4,
                          marginLeft: 0
                        },
                      }}
                      onDateChange={(c) => { this.setState({ date_end: c }) }}
                    /></View>

                </View>
                :
                <View >
                  <View style={{ borderWidth: 1, borderRadius: 3.5, marginTop: 10, borderColor: '#bbb' }}>
                    <DatePicker
                      style={{ width: '100%' }}
                      date={this.state.date_time}
                      mode="date"
                      placeholder="date"
                      confirmBtnText="Confirm"
                      cancelBtnText="Cancel"
                      customStyles={{
                        dateIcon: {
                          position: 'absolute',
                          right: 0,
                          top: 4,
                          marginLeft: 0
                        },
                      }}
                      onDateChange={(de) => { this.setState({ date_time: de }) }}
                    /></View>
                  <View style={{ flexDirection: 'row', marginTop: 10 }}>
                    <View style={{ borderWidth: 1, borderRadius: 3.5, borderColor: '#bbb', width: '47.5%', marginRight: '2.5%' }}>
                      <DatePicker
                        style={{ width: '100%' }}
                        date={this.state.time_from}
                        mode="time"
                        placeholder="time from"
                        confirmBtnText="Confirm"
                        cancelBtnText="Cancel"
                        customStyles={{
                          dateIcon: {
                            position: 'absolute',
                            right: 0,
                            top: 4,
                            marginLeft: 0
                          },
                        }}
                        onDateChange={(df) => { this.setState({ time_from: df }) }}
                      /></View>
                    <View style={{ borderWidth: 1, borderRadius: 3.5, borderColor: '#bbb', width: '47.5%', marginLeft: '2.5%' }}>
                      <DatePicker
                        style={{ width: '100%' }}
                        date={this.state.time_to}
                        mode="time"
                        placeholder="time to"
                        confirmBtnText="Confirm"
                        cancelBtnText="Cancel"
                        customStyles={{
                          dateIcon: {
                            position: 'absolute',
                            right: 0,
                            top: 4,
                            marginLeft: 0
                          },
                        }}
                        onDateChange={(dd) => { this.setState({ time_to: dd }) }}
                      /></View>
                  </View>
                </View>
              }

              <View style={{ marginTop: 20 }}>
                <TextInput style={{ borderWidth: 2, height: 100, paddingLeft: 10, paddingBottom: 50, borderColor: '#bbb', borderRadius: 3.5 }}
                  underlineColorAndroid='transparent'
                  placeholder="Leave Reason"
                  value={this.state.lea_reason}
                  onChangeText={(b) => this.setState({ lea_reason: b })}
                />
              </View>

              <View style={{ flexDirection: 'row', marginTop: 20 }} >
                <Button style={{ justifyContent: 'center', backgroundColor: 'white', borderRadius: 5, borderColor: '#00E0FF', width: '47.5%', marginRight: '2.5%', borderWidth: 2 }}
                  onPress={() => this.props.navigation.navigate('ap2')} ><Text style={{color:'#00E0FF'}}>Cancel</Text></Button>

                <Button style={{ justifyContent: 'center', backgroundColor: 'white', borderRadius: 5, borderColor: '#00E0FF', width: '47.5%', marginLeft: '2.5%', borderWidth: 2 }}
                  onPress={() => this.onPressSubmit()}><Text style={{color:'#00E0FF'}}>Submit</Text></Button>
              </View>
            </View>
          </View>
        </KeyboardAwareScrollView>
        <View style={{ flex: 0.21, backgroundColor: '#00CCFF',justifyContent:'center' }}>

          <Text style={{ fontSize: 15, color: 'white', textAlign: 'center'}} >PROJECTSOFT (Thailand)Co,Ltd.</Text>
        </View>
      </View>

    );
  }
}