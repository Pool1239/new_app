import React, { Component } from 'react';
import { View, Text, FlatList, TouchableOpacity, AsyncStorage, ScrollView, Image } from 'react-native'
import { Container, Header, Left, Right, Button, Icon, Title, Card, CardItem, Body, Content, Row } from 'native-base';
import { mockup } from '../data/MockupListview'
import ListLeave from '../component/ListLeave'
import { get, post, sever, getImg } from '../service/api'
import PhotoUpload from 'react-native-photo-upload';

export default class App2 extends Component {

  static navigationOptions = {
    header: null,
  }

  constructor(props) {
    super(props)
    this.state = {
      userdata: [],
      fullname: [],
      role: [],
      annual: [], timeannual: [], sumannual: '',
      personal: [], sumpersonal: '', timepersonal: [],
      sick: [], sumsick: '', timesickl: [],
      enter: [], sumenter: '', timeenter: [],
      military: [], summilitary: '', timemilitary: [],
      maternity: [], summaternity: '', timematernity: [],
      training: [], sumtraining: '', timetraining: [],
      image: '',
      avatar: '',
 
    }
  }

  async componentDidMount() {
    let res = await AsyncStorage.getItem("userdata")
    let userdata = JSON.parse(res)

    const image = await get('/user_profiles2', userdata.token)
    // this.setState({userdata})
    // console.log('nobar', userdata)
    this.setState({
      userdata: userdata,
      ...image,
      // userdata2: userdata,userdata3: userdata,userdata4: userdata,userdata5: userdata,userdata6: userdata,userdata7: userdata,
      fullname: userdata.result[0].full_name,
      role: userdata.result[0].role,
    })


    try {
      const res = await post('/show_my_leave_request_1', { empID: userdata.result[0].empID }, userdata.token)
      this.setState({ annual: res.result })
      const req = await post('/show_my_leave_request_1_1', { empID: userdata.result[0].empID }, userdata.token)
      this.setState({ timeannual: req.result })
      this.setState({ sumannual: parseFloat(this.state.annual) + parseFloat(this.state.timeannual) })
    } catch (error) {
      alert(error)
    }

    try {
      const res = await post('/show_my_leave_request_2', { empID: userdata.result[0].empID }, userdata.token)
      this.setState({ personal: res.result })
      const req = await post('/show_my_leave_request_2_1', { empID: userdata.result[0].empID }, userdata.token)
      this.setState({ timepersonal: req.result })
      this.setState({ sumpersonal: parseFloat(this.state.personal) + parseFloat(this.state.timepersonal) })
    } catch (error) {
      alert(error)
    }

    try {
      const res = await post('/show_my_leave_request_3', { empID: userdata.result[0].empID }, userdata.token)
      this.setState({ sick: res.result })
      const req = await post('/show_my_leave_request_3_1', { empID: userdata.result[0].empID }, userdata.token)
      this.setState({ timesickl: req.result })
      this.setState({ sumsick: parseFloat(this.state.sick) + parseFloat(this.state.timesickl) })
    } catch (error) {
      alert(error)
    }

    try {
      const res = await post('/show_my_leave_request_4', { empID: userdata.result[0].empID }, userdata.token)
      this.setState({ enter: res.result })
      const req = await post('/show_my_leave_request_4_1', { empID: userdata.result[0].empID }, userdata.token)
      this.setState({ timeenter: req.result })
      this.setState({ sumenter: parseFloat(this.state.enter) + parseFloat(this.state.timeenter) })
    } catch (error) {
      alert(error)
    }

    try {
      const res = await post('/show_my_leave_request_5', { empID: userdata.result[0].empID }, userdata.token)
      this.setState({ military: res.result })
      const req = await post('/show_my_leave_request_5_1', { empID: userdata.result[0].empID }, userdata.token)
      this.setState({ timemilitary: req.result })
      this.setState({ summilitary: parseFloat(this.state.military) + parseFloat(this.state.timemilitary) })
    } catch (error) {
      alert(error)
    }

    try {
      const res = await post('/show_my_leave_request_6', { empID: userdata.result[0].empID }, userdata.token)
      this.setState({ maternity: res.result })
      const req = await post('/show_my_leave_request_6_1', { empID: userdata.result[0].empID }, userdata.token)
      this.setState({ timematernity: req.result })
      this.setState({ summaternity: parseFloat(this.state.maternity) + parseFloat(this.state.timematernity) })
    } catch (error) {
      alert(error)
    }

    try {
      const res = await post('/show_my_leave_request_7', { empID: userdata.result[0].empID }, userdata.token)
      this.setState({ training: res.result })
      const req = await post('/show_my_leave_request_7_1', { empID: userdata.result[0].empID }, userdata.token)
      this.setState({ timetraining: req.result })
      this.setState({ sumtraining: parseFloat(this.state.training) + parseFloat(this.state.timetraining) })
    } catch (error) {
      alert(error)
    }

  }

  async _Goto_Leave() {
    let res = await AsyncStorage.getItem("userdata")
    let userdata = JSON.parse(res)
    const an = parseInt('1')
    const obj = { an }
    try {
      const res = await post('/show_my_leave_request_8', obj, userdata.token)
      AsyncStorage.setItem("an", JSON.stringify(res.result))
      this.props.navigation.navigate('ap3')
    } catch (error) {
      alert(error)
    }

  }

  async _Goto_Leave2() {
    let res = await AsyncStorage.getItem("userdata")
    let userdata = JSON.parse(res)
    const an = parseInt('2')
    const obj = { an }
    try {
      const res = await post('/show_my_leave_request_8', obj, userdata.token)
      AsyncStorage.setItem("an", JSON.stringify(res.result))
      this.props.navigation.navigate('ap3')
    } catch (error) {
      alert(error)
    }

  }

  async _Goto_Leave3() {
    let res = await AsyncStorage.getItem("userdata")
    let userdata = JSON.parse(res)
    const an = parseInt('3')
    const obj = { an }
    try {
      const res = await post('/show_my_leave_request_8', obj, userdata.token)
      AsyncStorage.setItem("an", JSON.stringify(res.result))
      this.props.navigation.navigate('ap3')
    } catch (error) {
      alert(error)
    }

  }

  async _Goto_Leave4() {
    let res = await AsyncStorage.getItem("userdata")
    let userdata = JSON.parse(res)
    const an = parseInt('4')
    const obj = { an }

    try {
      const res = await post('/show_my_leave_request_8', obj, userdata.token)
      AsyncStorage.setItem("an", JSON.stringify(res.result))
      this.props.navigation.navigate('ap3')
    } catch (error) {
      alert(error)
    }

  }

  async _Goto_Leave5() {
    let res = await AsyncStorage.getItem("userdata")
    let userdata = JSON.parse(res)
    const an = parseInt('5')
    const obj = { an }
    try {
      const res = await post('/show_my_leave_request_8', obj, userdata.token)
      AsyncStorage.setItem("an", JSON.stringify(res.result))
      this.props.navigation.navigate('ap3')
    } catch (error) {
      alert(error)
    }

  }

  async _Goto_Leave6() {
    let res = await AsyncStorage.getItem("userdata")
    let userdata = JSON.parse(res)
    const an = parseInt('6')
    const obj = { an }
    try {
      const res = await post('/show_my_leave_request_8', obj, userdata.token)
      AsyncStorage.setItem("an", JSON.stringify(res.result))
      this.props.navigation.navigate('ap3')
    } catch (error) {
      alert(error)
    }

  }

  async _Goto_Leave7() {
    let res = await AsyncStorage.getItem("userdata")
    let userdata = JSON.parse(res)
    const an = parseInt('7')
    const obj = { an }
    try {
      const res = await post('/show_my_leave_request_8', obj, userdata.token)
      AsyncStorage.setItem("an", JSON.stringify(res.result))
      this.props.navigation.navigate('ap3')
    } catch (error) {
      alert(error)
    }

  }
  async onPressA() {
      let ress = await AsyncStorage.getItem("userdata")
      let userdata = JSON.parse(ress)
      this.setState({
        userdata: userdata,

      })

      const empID = parseInt(userdata.result[0].empID)

      const { avatar } = this.state
      const obj = {
        empID, image: ',' + avatar
      }
      try {
        await post('/insert_profile', obj, userdata.token)
        alert("Update Success")
      } catch (error) {
        alert(error)
      }
    
  }

  render() {

    return (

      <View style={{ flex: 1 }} >
        <View style={{ alignItems: 'center', flex: 1, backgroundColor: '#00CCFF', justifyContent: 'center', height: 60 }} >
          <Text style={{ fontSize: 23, color: 'white' }} >Leave Request & Remaining</Text>
        </View>

        <View style={{ flex: 10, backgroundColor: '#ffffff', paddingBottom: 5, paddingHorizontal: 40 }} >

          <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 15, borderBottomWidth: 1, paddingBottom: 10, justifyContent: 'center' }}>
            <View style={{ alignItems: 'center', marginRight: 10, }}>

              <PhotoUpload
                onPhotoSelect={avatar => {
                  if (avatar) {
                    this.setState({
                      avatar
                    })
                    this.onPressA()
                  }
                }}
              >
                {
                  this.state.avatar ?
                    <Image style={{width: 50, height: 50, borderRadius: 22}}/>
                    :
                    <Image style={{ width: 50, height: 50, borderRadius: 22 }} source={this.state.image ? { uri: sever + this.state.image } : null} />
                }
              </PhotoUpload>

            </View>
            <View>
              <Text style={{ fontSize: 15, color: '#00CCFF' }}>{this.state.role}</Text>
              <Text style={{ fontSize: 20, fontWeight: 'bold' }} >{this.state.fullname}</Text>
            </View>
          </View>


          <Text style={{ textAlign: 'center', fontSize: 17, color: '#00CCFF', marginTop: 10, marginBottom: 10 }} >Public Holiday</Text>

          <ScrollView style={{ height: 250 }}>
            <View>
              <TouchableOpacity style={{ height: 90, backgroundColor: '#F5F5F5' }} onPress={() => this._Goto_Leave()}>
                <Card  >
                  <CardItem style={{ justifyContent: 'space-between' }} >
                    <View>
                      <Text style={{ fontWeight: 'bold' }} >
                        Annual Leave
                  </Text>
                    </View>
                    <View style={{ flexDirection: 'row' }}>
                      <View>
                        <Text style={{ color: '#00CCFF' }} >
                          {this.state.sumannual + "/12"}
                        </Text>
                      </View>
                      <View style={{ marginLeft: 10 }} >
                        <Text  >
                          Days
                  </Text>
                      </View>
                    </View>
                  </CardItem>
                  <Text style={{ marginLeft: 20 }} >
                    ลาพักร้อน
                </Text>
                </Card>
              </TouchableOpacity>
            </View>
            {/* ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// */}
            <View>
              <TouchableOpacity style={{ height: 90, backgroundColor: '#F5F5F5' }} onPress={() => this._Goto_Leave2()}>
                <Card  >
                  <CardItem style={{ justifyContent: 'space-between' }} >
                    <View>
                      <Text style={{ fontWeight: 'bold' }} >
                        Personal Leave
                      </Text>
                    </View>
                    <View style={{ flexDirection: 'row' }}>
                      <View>
                        <Text style={{ color: '#00CCFF' }} >
                          {this.state.sumpersonal + "/5"}
                        </Text>
                      </View>
                      <View style={{ marginLeft: 10 }} >
                        <Text >
                          Days
                        </Text>
                      </View>
                    </View>
                  </CardItem>
                  <Text style={{ marginLeft: 20 }} >
                    ลากิจ
                </Text>
                </Card>
              </TouchableOpacity>
            </View>
            {/* /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// */}
            <View>
              <TouchableOpacity style={{ height: 90, backgroundColor: '#F5F5F5' }} onPress={() => this._Goto_Leave3()}>
                <Card  >
                  <CardItem style={{ justifyContent: 'space-between' }} >
                    <View>
                      <Text style={{ fontWeight: 'bold' }} >
                        Sick leave
                      </Text>
                    </View>
                    <View style={{ flexDirection: 'row' }}>
                      <View>
                        <Text style={{ color: '#00CCFF' }} >
                          {this.state.sumsick + "/30"}
                        </Text>
                      </View>
                      <View style={{ marginLeft: 10 }} >
                        <Text >
                          Days
                      </Text>
                      </View>
                    </View>
                  </CardItem>
                  <Text style={{ marginLeft: 20 }} >
                    ลาป่วย
                </Text>
                </Card>
              </TouchableOpacity>
            </View>
            {/* ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// */}
            <View>
              <TouchableOpacity style={{ height: 90, backgroundColor: '#F5F5F5' }} onPress={() => this._Goto_Leave4()}>
                <Card  >
                  <CardItem style={{ justifyContent: 'space-between' }} >
                    <View>
                      <Text style={{ fontWeight: 'bold' }} >
                        Enter for monkhodd
                      </Text>
                    </View>
                    <View style={{ flexDirection: 'row' }}>
                      <View>
                        <Text style={{ color: '#00CCFF' }} >
                          {this.state.sumenter + "/10"}
                        </Text>
                      </View>
                      <View style={{ marginLeft: 10 }} >
                        <Text  >
                          Days
                      </Text>
                      </View>
                    </View>
                  </CardItem>
                  <Text style={{ marginLeft: 20 }} >
                    ลาบวช
                </Text>
                </Card>
              </TouchableOpacity>
            </View>
            {/* ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// */}
            <View>
              <TouchableOpacity style={{ height: 90, backgroundColor: '#F5F5F5' }} onPress={() => this._Goto_Leave5()}>
                <Card  >
                  <CardItem style={{ justifyContent: 'space-between' }} >
                    <View>
                      <Text style={{ fontWeight: 'bold' }} >
                        Military Leave
                      </Text>
                    </View>
                    <View style={{ flexDirection: 'row' }}>
                      <View>
                        <Text style={{ color: '#00CCFF' }} >
                          {this.state.summilitary + "/60"}
                        </Text>
                      </View>
                      <View style={{ marginLeft: 10 }} >
                        <Text >
                          Days
                      </Text>
                      </View>
                    </View>
                  </CardItem>
                  <Text style={{ marginLeft: 20 }} >
                    ลาเพื่อรับราชการทหาร
                </Text>
                </Card>
              </TouchableOpacity>
            </View>
            {/* /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// */}
            <View>
              <TouchableOpacity style={{ height: 90, backgroundColor: '#F5F5F5' }} onPress={() => this._Goto_Leave6()}>
                <Card  >
                  <CardItem style={{ justifyContent: 'space-between' }} >
                    <View>
                      <Text style={{ fontWeight: 'bold' }} >
                        Maternity Leave
                      </Text>
                    </View>
                    <View style={{ flexDirection: 'row' }}>
                      <View>
                        <Text style={{ color: '#00CCFF' }} >
                          {this.state.summaternity + "/90"}
                        </Text>
                      </View>
                      <View style={{ marginLeft: 10 }} >
                        <Text >
                          Days
                      </Text>
                      </View>
                    </View>
                  </CardItem>
                  <Text style={{ marginLeft: 20 }} >
                    ลาคลอด
                </Text>
                </Card>
              </TouchableOpacity>
            </View>
            {/* ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// */}
            <View>
              <TouchableOpacity style={{ height: 90, backgroundColor: '#F5F5F5' }} onPress={() => this._Goto_Leave7()}>
                <Card  >
                  <CardItem style={{ justifyContent: 'space-between' }} >
                    <View>
                      <Text style={{ fontWeight: 'bold' }} >
                        Training Leave
                      </Text>
                    </View>
                    <View style={{ flexDirection: 'row' }}>
                      <View>
                        <Text style={{ color: '#00CCFF' }} >
                          {this.state.sumtraining + "/5"}
                        </Text>
                      </View>
                      <View style={{ marginLeft: 10 }} >
                        <Text  >
                          Days
                      </Text>
                      </View>
                    </View>
                  </CardItem>
                  <Text style={{ marginLeft: 20 }} >
                    ลาเพื่อฝึกอบรม
                </Text>
                </Card>
              </TouchableOpacity>
            </View>
            {/* /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// */}
            <View>
              <TouchableOpacity style={{ height: 90, backgroundColor: '#F5F5F5' }} onPress={() => this.props.navigation.navigate('ap4')}>
                <Card  >
                  <CardItem>
                    <Text style={{ fontWeight: 'bold' }} >
                      Flexlble Benefit Request
                  </Text>
                    <Text style={{ color: '#00CCFF', marginRight: 5 }} >

                    </Text>
                    <Text  >

                    </Text>
                  </CardItem>
                  <Text style={{ marginLeft: 20 }} >
                    คำขอสวัสดิการอื่่นๆ
                </Text>
                </Card>
              </TouchableOpacity>
            </View>
          </ScrollView>
          {/* ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// */}
        </View>
        <View style={{ alignItems: 'center', flex: 1, backgroundColor: '#00CCFF', justifyContent: 'center', height: 40 }} >
          <Text style={{ fontSize: 15, color: 'white' }} >PROJECTSOFT(THAILAND)CO.,LTD.</Text>
        </View>

      </View>

    );
  }
}