import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, Image, TextInput, AsyncStorage } from 'react-native';
import { Button, CardItem } from 'native-base'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { post } from '../service/api'
import { StackActions, NavigationActions } from 'react-navigation';

export default class d1 extends React.Component {
    static navigationOptions = {
        // title: 'Login',
        header: null,
    }

    constructor(props) {
        super(props)
        this.state = {
            userName: "",
            password: "",
        }
    }

    async componentDidMount() {
        let res = await AsyncStorage.getItem("userdata")
        if(res){
            const resetAction = StackActions.reset({
                index: 0,
                actions: [NavigationActions.navigate({ routeName: 'ap2' })],
            });
            this.props.navigation.dispatch(resetAction);
        }
        
    }

    async onPressLogin() {
        if (this.state.userName == "" || this.state.password == "") { alert("กรุณากรอกข้อมูลให้ครบ"); }
        else {
            const { userName, password } = this.state
            const obj = {
                userName, password
            }
            console.log(obj)
            try {
                let result = await post('/mobile_login', obj)
                // this.setState({ data : result.result })
                AsyncStorage.setItem("userdata", JSON.stringify(result))
                console.log(result)
                if (result.success) {
                    const resetAction = StackActions.reset({
                        index: 0,
                        actions: [NavigationActions.navigate({ routeName: 'ap2' })],
                    });
                    this.props.navigation.dispatch(resetAction);
                }
                else {
                    alert(result.massage = "login fail")
                }
            } catch (error) {
                alert(error)
            }
        }
    }


    render() {
        return (
            <KeyboardAwareScrollView contentContainerStyle={{ flex: 1 }} keyboardShouldPersistTaps={'handled'}>
                <View style={{flexDirection: 'column',alignItems: 'center',flex:1,justifyContent:'center',paddingHorizontal:75}}>

                   
                    <Image
                        source={require('../assets/log.png')}
                    />


                    <TextInput style={styles.tp1} underlineColorAndroid='transparent'
                        value={this.state.userName}
                        onChangeText={(e) => this.setState({ userName: e })}
                        onSubmitEditing={() => { this.secondTextInput.focus(); }}
                        placeholder="Username"

                    />
                    <TextInput style={styles.tp} underlineColorAndroid='transparent' secureTextEntry={true}
                        value={this.state.password}
                        onChangeText={(e) => this.setState({ password: e })}
                        ref={(input) => { this.secondTextInput = input; }}
                        placeholder="Password"
                    />
                    <Button style={styles.bt} onPress={() => this.onPressLogin()}>
                        <Text style={{ color: '#00E0FF' }}>Login </Text>
                    </Button>

                    {/* <Button style={styles.bt} onPress={() => this.props.navigation.navigate('s')}>
                        <Text style={{ color: '#00E0FF' }}>Sign Up </Text>
                    </Button> */}
                    <View style={{ marginTop: 15 }}>
                        <Button transparent
                            onPress={() => this.props.navigation.navigate('p')}

                        ><Text style={{ color: '#00CCFF' }}>Forgot your Password?</Text></Button>
                    </View>
                    
                </View>
            </KeyboardAwareScrollView>
        );
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F3F3F3',
    },

    welcome: {
        fontSize: 15,
        color: '#33B2FF',
        textAlign: 'center',
        margin: 50,
    },
    welcome1: {
        fontSize: 20,
        color: '#33B2FF',
        textAlign: 'center',
        margin: 20,
    },
    tp: {
        width: '100%',
        height: 40,
        backgroundColor: "#FFFFFF",
        marginBottom: 40,
        color: 'black',
        borderColor: '#CBCBCB',
        borderWidth: 1,
        paddingLeft: 20

    },
    tp1: {
        width: '100%',
        height: 40,
        backgroundColor: "#FFFFFF",
        marginBottom: 10,
        marginTop: 40,
        borderColor: '#CBCBCB',
        borderWidth: 1,
        paddingLeft: 20

    },
    bt: {
        borderWidth: 2,
        borderColor: '#00E0FF',
        backgroundColor: 'white',
        width: '100%',
        height: 40,
        justifyContent: 'center',
        alignSelf: 'center',
        marginBottom: 15,
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
});